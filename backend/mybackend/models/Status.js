const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.connect('mongodb://localhost/small_pro', { useNewUrlParser: true })
const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function() {
    console.log('connectstatus')
})


const statusSchema = new Schema({
    name: String,
    status: Boolean,


})

const Status = mongoose.model('Status', statusSchema)

Status.find(function(err, Status) {
    if (err) return console.log(err)
    console.log(Status)
})
module.exports = Status