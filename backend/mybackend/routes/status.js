const express = require('express');
const Status = require('../models/Status.js');
const statusController = require('../controller/StatusController');
const router = express.Router();

/* GET users listing. */


router.get('/', statusController.getStatus)
router.post('/', statusController.addStatus)

router.put('/', statusController.updateStatus)









module.exports = router;