const express = require('express')
const router = express.Router()
const Customer = require('../models/Customer.js');
const customersController = require('../controller/CustomersController')

/* GET customers listing. */
router.get('/', customersController.getCustomers)

router.get('/:id', customersController.getCustomer)

router.post('/', customersController.addCustomer)

router.put('/', customersController.updateCustomer)

router.delete('/:id', customersController.deleteCustomer)

module.exports = router